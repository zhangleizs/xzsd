package com.neusoft.bookstore.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 74085
 */
@Data
public class BaseTree {
    @ApiModelProperty("树节点名称")
    private String nodeName;
    @ApiModelProperty("节点Id:唯一表示树的节点信息")
    private String nodeId;

    @ApiModelProperty("节点url")
    private String nodeUrl;

    @ApiModelProperty("该节点所有属性信息")
    private  Object attribute;

    @ApiModelProperty("该节点下子节点信息")
    private List<BaseTree> childNodes;
}
