package com.neusoft.bookstore.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/*Serializable便于前后端交互*/
@Data
public class BaseModel implements Serializable {
    @ApiModelProperty("主键ID")
    private  Integer     id;
    @ApiModelProperty("作废标记0：否（未删除）1：是（已删除）")
    private  Integer    isDelete;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人（当前登录用户的用户账号 如果不存在取固定值“asmin”）")
    private  String     createdBy;
    @ApiModelProperty("更新时间")
    private  Date       updateTime;
    @ApiModelProperty("更新人当前登录用户的用户账号 如果不存在取固定值“asmin”）")
    private  String     updateBy;
    @ApiModelProperty("当前页")
    private  Integer pageNum;
    @ApiModelProperty("每页显示条数")
    private  Integer pageSize;
    @ApiModelProperty("获取从前台的用户账号或手机号")
    private  String loginAccount;
}
