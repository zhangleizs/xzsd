package com.neusoft.bookstore.menu.controller;

import com.neusoft.bookstore.customer.model.Customer;
import com.neusoft.bookstore.menu.model.Menu;
import com.neusoft.bookstore.menu.service.MenuService;
import com.neusoft.bookstore.util.ErrorCode;
import com.neusoft.bookstore.util.ResponseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 74085
 */
@RequestMapping("menu")
@RestController
@Api("menu")
public class MenuController {
    @Autowired
    private MenuService menuService;
    @Autowired
    private  ResponseVo responseVo;

    @ApiOperation(value="新增菜单",notes = "新增菜单")
    //@RequestMapping(value = "addCustomer",method = RequestMethod.POST)
    @PostMapping("addMenu")
    public ResponseVo addCustomer(Menu menu)
    {

        try {
            responseVo=menuService.addMenu(menu);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }

        return   responseVo;
    }

    @ApiOperation(value="菜单树查询",notes = "菜单树查询")
    //@RequestMapping(value = "addCustomer",method = RequestMethod.POST)
    @GetMapping("listMenuTree")
    public ResponseVo addCustomer()
    {

        try {
            responseVo=menuService.listMenuTree();
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }

        return   responseVo;
    }

    @ApiOperation(value="菜单详情查询",notes = "菜单详情查询")
    //@RequestMapping(value = "addCustomer",method = RequestMethod.POST)
    @GetMapping("findMenuTree")
    public ResponseVo findMenuTree(String menuCode)
    {

        try {
            responseVo=menuService.findMenuTree(menuCode);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }

        return   responseVo;
    }


    @ApiOperation(value="修改菜单信息",notes = "修改菜单信息")
    //@RequestMapping(value = "addCustomer",method = RequestMethod.POST)
    @PostMapping("updateMenuByCode")
    public ResponseVo findMenuTree(Menu menu)
    {

        try {
            responseVo=menuService.updateMenuByCode(menu);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }

        return   responseVo;
    }




    @ApiOperation(value="删除菜单信息",notes = "删除菜单信息")
    //@RequestMapping(value = "addCustomer",method = RequestMethod.POST)
    @GetMapping("deleteMenuByCode")
    public ResponseVo deleteMenuByCode(String menuCode,String loginAccount)
    {

        try {
            responseVo=menuService.deleteMenuByCode(menuCode,loginAccount);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }

        return   responseVo;
    }
}
