package com.neusoft.bookstore.menu.service;

import com.neusoft.bookstore.menu.model.Menu;
import com.neusoft.bookstore.util.ResponseVo;

public interface MenuService {
    ResponseVo addMenu(Menu menu);

    ResponseVo listMenuTree();

    ResponseVo findMenuTree(String menuCode);

    ResponseVo updateMenuByCode(Menu menu);

    ResponseVo deleteMenuByCode(String menuCode,String loginAccount);
}
