package com.neusoft.bookstore.menu.service.impl;

import com.neusoft.bookstore.customer.model.Customer;
import com.neusoft.bookstore.menu.mapper.MenuMapper;
import com.neusoft.bookstore.menu.model.Menu;
import com.neusoft.bookstore.menu.service.MenuService;
import com.neusoft.bookstore.util.BaseTree;
import com.neusoft.bookstore.util.ErrorCode;
import com.neusoft.bookstore.util.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import sun.nio.cs.ArrayEncoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Service
public class MenuServicelmpl  implements MenuService {
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseVo addMenu(Menu menu) {
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "新增失败");
        Customer o = (Customer) redisTemplate.opsForValue().get(menu.getLoginAccount());
        if (o!=null)
        {
            menu.setCreatedBy(o.getUserAccount());
        }
        else
        {
            responseVo.setMsg("请登录后重试");
            return  responseVo;
        }
        //规定一级菜单父级为0
        String frontMenuCode = menu.getFrontMenuCode();
        if(StringUtils.isEmpty(frontMenuCode))
        {
            //没有点击 创建的一级菜单
            menu.setParentMenuCode("0");
        }
        else
        {
            menu.setParentMenuCode(frontMenuCode);
        }

        Menu menuByParentMenuCodeAndName = menuMapper.findMenuByParentMenuCodeAndName(menu);

        if(menuByParentMenuCodeAndName!=null)
        {
            responseVo.setMsg("当前级别的菜单下名称重复!");
            return  responseVo;
        }

        if(!"0".equals(menu.getFrontMenuCode()))
        {
            //更新操作
            //查询 更改父级菜单类型
            Menu menu2=menuMapper.findMenuByMenuCode(frontMenuCode);
            if(menu2!=null){
                if(menu2.getType()==2)
                {
                  //更新 修改为目录，清空menu_url

                    int i=menuMapper.updateTypeAndUrlByCode(frontMenuCode);
                }
        }

        }
        int result=menuMapper.insertMenu(menu);
        if(result==1)
        {
            responseVo.setMsg("新增成功！");
            responseVo.setCode(ErrorCode.SUCCESS);
            responseVo.setSuccess(true);
             return  responseVo;
        }

        return responseVo;
    }

    @Override
    public ResponseVo listMenuTree() {

        /*
        * 找出所有菜单目录
        * 将菜单信息有层级关系的树信息（菜单信息转化为树信息，然后将树返回）
        * */
        ResponseVo responseVo = new ResponseVo(true,ErrorCode.SUCCESS, "查询成功");
        //找出所有菜单
         List<Menu> menus=menuMapper.listMenus();


         if(menus==null ||menus.size()==0)
         {
             responseVo.setMsg("未查询到任何信息");
             return  responseVo;
         }
        // 将菜单信息有层级关系的树信息（菜单信息转化为树信息，然后将树返回）

        //先创建一课树
        BaseTree rootTree = new BaseTree();
         //规定根节点为0
         String rootNodeId="0";

         initTree(rootTree,menus,rootNodeId);
        List<BaseTree> childNodes = rootTree.getChildNodes();
        responseVo.setData(childNodes);
        return responseVo;
    }

    @Override
    public ResponseVo findMenuTree(String menuCode) {
        ResponseVo responseVo = new ResponseVo(true, ErrorCode.SUCCESS, "查询成功");
          if(StringUtils.isEmpty(menuCode))
          {
              responseVo.setMsg("菜单编码不能为空");
              responseVo.setCode(ErrorCode.FAIL);
              responseVo.setSuccess(false);
              return  responseVo;
          }
        Menu menu = menuMapper.findMenuByMenuCode(menuCode);
          if(menu==null)
          {
              responseVo.setMsg("未查询到信息");
              return  responseVo;
          }

          responseVo.setData(menu);


        return responseVo;
    }

    @Override
    public ResponseVo updateMenuByCode(Menu menu) {
          //菜单名不重复
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "修改失败");
        Customer o = (Customer) redisTemplate.opsForValue().get(menu.getLoginAccount());
        if (o!=null)
        {
            menu.setCreatedBy(o.getUserAccount());
        }
        else
        {
            responseVo.setMsg("请登录后重试");
            return  responseVo;
        }
        Menu menuByParentMenuCodeAndName = menuMapper.findMenuByParentMenuCodeAndName(menu);
        if(menuByParentMenuCodeAndName!=null)
        {
            responseVo.setMsg("名字重复了");
            return  responseVo;
        }
        int i=menuMapper.updateMenuByCode(menu);
        if(i==1)
        {
            responseVo.setMsg("修改成功");
            responseVo.setSuccess(true);
            responseVo.setCode(ErrorCode.SUCCESS);
            return  responseVo;
        }
        return responseVo;
    }

    @Override
    public ResponseVo deleteMenuByCode(String menuCode ,String loginAccount) {
        ResponseVo responseVo = new ResponseVo(true, ErrorCode.SUCCESS, "删除成功");

        Customer customer = (Customer) redisTemplate.opsForValue().get(loginAccount);
        if(customer==null)
        {
            responseVo.setMsg("请先登录");
            return  responseVo;
        }
        String loginAccount1=customer.getUserAccount();
        
        
        

        if(StringUtils.isEmpty(menuCode))
        {
            responseVo.setMsg("菜单编码不能为空");
            responseVo.setCode(ErrorCode.FAIL);
            responseVo.setSuccess(false);
            return  responseVo;
        }
        List<Menu> menus= menuMapper.findChildMenus(menuCode);
        if(menus!=null&&menus.size()>0)
        {
            //有子集
            responseVo.setSuccess(false);
            responseVo.setMsg("当前菜单有子集不能删除!");
            responseVo.setCode(ErrorCode.FAIL);
            return  responseVo;
        }

        HashMap<String, String> map = new HashMap<>();
        map.put("menuCode",menuCode);
        map.put("loginAccount",loginAccount1);
        int i= menuMapper.deleteMenuByCode(map);
        if(i!=1)
        {
            responseVo.setMsg("删除失败");
            responseVo.setSuccess(true);
            responseVo.setCode(ErrorCode.FAIL);
            return  responseVo;
        }


        return responseVo;
    }

    private void initTree(BaseTree rootTree, List<Menu> menus, String rootNodeId) {
        Iterator<Menu> iterator = menus.iterator();
        while(iterator.hasNext())
        {
            /*
            *
            *
            *
            * */

            Menu menu = iterator.next();
            if(menu.getMenuCode().equals(rootNodeId))
            {
                //创建根树
                menuToTree(rootTree,menu);
            }

            else if (menu.getParentMenuCode().equals(rootNodeId))
            {
                //子节点 创建子树
                BaseTree childTree = new BaseTree();
                menuToTree(childTree,menu);
                 //需要将子树加入根树
                 //需要判断 根节点是否已经创建
                if (childTree.getNodeId()!=null)
                {
                    if(rootTree.getChildNodes()==null)
                    {
                        ArrayList<BaseTree> list = new ArrayList<>();
                        rootTree.setChildNodes(list);
                    }
                    rootTree.getChildNodes().add(childTree);

                }
                initTree(childTree,menus,menu.getMenuCode());
            }
        }

    }

    private void menuToTree(BaseTree rootTree, Menu menu) {
        rootTree.setNodeId(menu.getMenuCode());
        rootTree.setNodeName(menu.getMenuName());
        rootTree.setNodeUrl(menu.getMenuUrl());
        rootTree.setAttribute(menu);

    }


}
