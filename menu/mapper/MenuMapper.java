package com.neusoft.bookstore.menu.mapper;

import com.neusoft.bookstore.menu.model.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

/**
 * @author 74085
 */
@Mapper
public interface MenuMapper {
    int updateTypeAndUrlByCode(String frontMenuCode);

    Menu findMenuByParentMenuCodeAndName(Menu menu);

    Menu findMenuByMenuCode(String frontMenuCode);

    int insertMenu(Menu menu);

    List<Menu> listMenus();

    int updateMenuByCode(Menu menu);

    List<Menu> findChildMenus(String menuCode);

    int deleteMenuByCode(HashMap<String, String> menuCode);
}
