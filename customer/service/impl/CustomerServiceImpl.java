package com.neusoft.bookstore.customer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.neusoft.bookstore.customer.mapper.CustomerMapper;
import com.neusoft.bookstore.customer.model.Customer;
import com.neusoft.bookstore.customer.service.CustomerService;

import com.neusoft.bookstore.util.ErrorCode;
import com.neusoft.bookstore.util.MD5Util;

import com.neusoft.bookstore.util.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;


@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerMapper customerMapper;
   /* @Autowired
    private  ResponseVo responseVo;*/
   @Autowired
   private RedisTemplate redisTemplate;
    @Override
    public ResponseVo addCustomer(Customer customer) {
        /**
         *   用户新增 ：
         *     1： 需要校验前台输入的用户名（用户账号）和手机好在系统中是否唯一
         *     2： 我们需要校验 是app注册 还是pc 注册 用过isAdmin（前台给值） 只需要校验 isAdmin是否规范（0或者1）
         *     3： 用户输入的密码 需要加密  MD5
         *     4：还要处理用户输入的金额（类型转换  String -->BigDecimal）  JSON
         */
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL,"新增失败");

        Customer customerByDb =customerMapper.findCustomerByPhoneAndAccount(customer);
      if(customerByDb!=null)
      {
          //数据库值重复
          //System.out.println("注册失败，用户账号或手机号已存在，请检查后重试！");
         responseVo.setMsg("注册失败，用户账号或手机号已存在，请检查后重试！");
          //return  responseVo;
      }

        /* String a;
       if(a!=null&& !"".equals(a))*/
        Integer isAdmin=customer.getIsAdmin();
        if(!StringUtils.isEmpty(isAdmin))
        {
            if(isAdmin!=0&&isAdmin!=1)
            {
                //System.out.println("isAdmin值不规范，请检查！");

                responseVo.setMsg("isAdmin值不规范，请检查！");
                //return  responseVo;

            }
        }

        //Md5加密
        String password=customer.getPassword();
        String inputPassToFormPass = MD5Util.inputPassToFormPass(password);
        customer.setPassword(inputPassToFormPass);

        //处理用户输入的金额
        BigDecimal score = new BigDecimal(customer.getFrontScore());
        customer.setScore(score);

        customer.setCreatedBy("admin");
        Customer o = (Customer) redisTemplate.opsForValue().get(customer.getLoginAccount());
           if (o!=null)
           {
               customer.setCreatedBy(o.getUserAccount());
           }

        int res= customerMapper.addCustomer(customer);
       if(res!=1)
       {
           //System.out.println();
           responseVo.setMsg("新增失败");
           //return  responseVo;


       }
       else
       {
           //System.out.println("新增成功");
           //result="新增成功";
           responseVo.setMsg("新增成功");
           responseVo.setCode(ErrorCode.SUCCESS);
           responseVo.setSuccess(true);

       }

       return  responseVo;

    }

    @Override
    public ResponseVo login(Customer customer) {
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "登陆失败");
        //校验数据非空  账号信息，密码信息
        if(StringUtils.isEmpty(customer.getLoginAccount()))
        {
            //账号信息为空
            responseVo.setMsg("用户账号或者手机不能为空");
            return  responseVo;
        }
        if(StringUtils.isEmpty(customer.getPassword()))
        {
            //账号信息为空
            responseVo.setMsg("用户密码不能为空");
            return  responseVo;
        }
        String password=customer.getPassword();
        String s = MD5Util.inputPassToFormPass(password);
        customer.setPassword(s);

        //数据库匹配
       Customer mer= customerMapper.selectLoginCustomer(customer);
       if(mer!=null)
       {
           responseVo.setMsg("登录成功");
           responseVo.setCode(ErrorCode.SUCCESS);
           responseVo.setSuccess(true);
           responseVo.setData(mer);
           //确定key和value
           redisTemplate.opsForValue().set(mer.getUserAccount(),mer);
           return  responseVo;
       }
        return responseVo;
    }

    @Override
    public ResponseVo loginOut(String userAccount) {
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "退出失败");
        if(StringUtils.isEmpty(userAccount))
        {
            responseVo.setMsg("用户信息不完整，退出失败");
            return  responseVo;
        }
        //清空相应redis
        Boolean delete = redisTemplate.delete(userAccount);
        if(delete)
        {
            responseVo.setMsg("退出成功");
            responseVo.setCode(ErrorCode.SUCCESS);
            responseVo.setSuccess(true);
            return responseVo;
        }

        return responseVo;
    }

    @Override
    public ResponseVo listCustomers(Customer customer) {

        ResponseVo responseVo = new ResponseVo(true, ErrorCode.SUCCESS, "查询成功");
        //添加分页插件
        PageHelper.startPage(customer.getPageNum(),customer.getPageSize());
        /*
         * 1:查询用户所有信息*/
        List<Customer>  customers=customerMapper.listCustomers(customer);
        //封装分页返回信息
        PageInfo<Customer> pageInfo=new PageInfo<>(customers);
        responseVo.setData(pageInfo);
         return responseVo;

    }

    @Override
    public ResponseVo queryUserDetails(Integer id) {
        ResponseVo responseVo = new ResponseVo(true, ErrorCode.SUCCESS, "查询成功");
        if(id==null)
        {
            responseVo.setMsg("查询失败");
            responseVo.setCode(ErrorCode.FAIL);
            responseVo.setSuccess(false);
            return  responseVo;
        }
        Customer customer= customerMapper.queryUserDetails(id);
        if(customer==null)
        {
            responseVo.setMsg("未查询到用户信息");
            responseVo.setCode(ErrorCode.FAIL);
            responseVo.setSuccess(false);
            return  responseVo;
        }

        responseVo.setData(customer);


        return responseVo;
    }

    /*
    * 根据用户id进行修改
    * */
    @Override
    public ResponseVo updateCustomerById(Customer customer) {
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "修改失败");

        List<Customer>  customerByDb = customerMapper.findCustomerByPhoneAndAccountExOwn(customer);
        if(customerByDb.size()>0)
        {
            //数据库值重复
            //System.out.println("注册失败，用户账号或手机号已存在，请检查后重试！");
            responseVo.setMsg("修改失败，用户账号或手机号已存在，请检查后重试！");

            return  responseVo;
        }

        //处理用户输入的金额
        BigDecimal score = new BigDecimal(customer.getFrontScore());
        customer.setScore(score);
        Customer o = (Customer) redisTemplate.opsForValue().get(customer.getLoginAccount());
        if (o!=null)
        {
            customer.setCreatedBy(o.getUserAccount());
        }
        else
        {
            customer.setCreatedBy("admin");
        }

          int i=customerMapper.updateCustomerById(customer);
        if(i==1)
        {
            responseVo.setSuccess(true);
            responseVo.setMsg("修改成功");
            responseVo.setCode(ErrorCode.SUCCESS);
            return  responseVo;
        }
        return responseVo;
    }

    @Override
    public ResponseVo deleteCustomerById(Integer id) {
        ResponseVo responseVo = new ResponseVo(true, ErrorCode.SUCCESS, "删除成功");
        if(id==null)
        {
            responseVo.setMsg("用户id不能为空");
            responseVo.setCode(ErrorCode.FAIL);
            responseVo.setSuccess(false);
            return  responseVo;
        }
        int i=customerMapper.deleteCustomerById(id);
        if(i!=1)
        {

            responseVo.setSuccess(false);
            responseVo.setMsg("删除失败");
            responseVo.setCode(ErrorCode.FAIL);
            return  responseVo;
        }
        return responseVo;
    }

    @Override
    public ResponseVo updatePwd(String originPwd, String newPwd, Integer userId, String userAccount) {
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "修改失败");
        if(StringUtils.isEmpty(originPwd)||StringUtils.isEmpty(newPwd)||userId==null||StringUtils.isEmpty(userAccount))
        {
            responseVo.setMsg("密码或账号信息为空！");
        }
        Customer customer = customerMapper.queryUserDetails(userId);
        if (customer==null)
        {
            responseVo.setMsg("用户信息不存在");
            return  responseVo;
        }

        String passToFormPass = MD5Util.inputPassToFormPass(originPwd);
        String password = customer.getPassword();
        /*
        * == 值和引用比较
        * equals 值比较
        * */
        if(!passToFormPass.equals(password))
        {
           responseVo.setMsg("原始密码不正确");
           return  responseVo;
        }
        //走修改
        Customer o = (Customer) redisTemplate.opsForValue().get(userAccount);
        if (o!=null)
        {
            userAccount=o.getUserAccount();
        }
        else
        {
          userAccount="admin";
        }


         passToFormPass = MD5Util.inputPassToFormPass(newPwd);
         password = passToFormPass;
        HashMap<Object,Object> hashMap = new HashMap();
        hashMap.put("p",password);
        hashMap.put("id",userId);
        hashMap.put("account",userAccount);
        int i=customerMapper.updatePwdById(hashMap);
        if(i==1)
        {
            responseVo.setMsg("密码修改成功");
            responseVo.setCode(ErrorCode.SUCCESS);
            responseVo.setSuccess(true);
            return  responseVo;
        }
        return  responseVo;
    }

    @Override
    public ResponseVo updateScore(String newScore, Integer userId, String userAccount) {
        ResponseVo responseVo = new ResponseVo(false, ErrorCode.FAIL, "修改失败");
        /*
        *通过传入登录人的用户名，来作为key获取redis的值
        * */
        Customer o = (Customer) redisTemplate.opsForValue().get(userAccount);
        if (o!=null)
        {
            userAccount=o.getUserAccount();
        }
        else
        {
            userAccount="admin";
        }

        if(StringUtils.isEmpty(newScore))
        {
            responseVo.setMsg("金额为空！");
        }
        Customer customer = customerMapper.queryUserDetails(userId);
        if (customer==null)
        {
            responseVo.setMsg("用户信息不存在");
            return  responseVo;
        }
        //处理用户输入的金额
        BigDecimal score = new BigDecimal(newScore);

        HashMap<Object, Object> map = new HashMap<>();
        map.put("newScore",score);
        map.put("userId",userId);
        map.put("userAccount",userAccount);
        int i= customerMapper.updateScore(map);
        if(i==1)
        {
            responseVo.setSuccess(true);
            responseVo.setCode(ErrorCode.SUCCESS);
            responseVo.setMsg("金额修改成功");
            return  responseVo;
        }



        return  responseVo;
    }
}
