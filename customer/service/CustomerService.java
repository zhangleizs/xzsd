package com.neusoft.bookstore.customer.service;

import com.neusoft.bookstore.customer.model.Customer;
import com.neusoft.bookstore.util.ResponseVo;

public interface CustomerService {
    ResponseVo addCustomer(Customer customer);

    ResponseVo login(Customer customer);

    ResponseVo loginOut(String userAccount);

    ResponseVo listCustomers(Customer customer);

    ResponseVo queryUserDetails(Integer id);

    ResponseVo updateCustomerById(Customer customer);

    ResponseVo deleteCustomerById(Integer id);

    ResponseVo updatePwd(String originPwd, String newPwd, Integer userId, String userAccount);

    ResponseVo updateScore(String newScore, Integer userId, String userAccount);
}
