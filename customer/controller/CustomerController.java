package com.neusoft.bookstore.customer.controller;

import com.neusoft.bookstore.customer.model.Customer;
import com.neusoft.bookstore.customer.service.CustomerService;
import com.neusoft.bookstore.util.ErrorCode;
import com.neusoft.bookstore.util.ResponseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/*@Controller
@ResponseBody*/
@RequestMapping("customer")
@RestController
@Api("customer")
public class CustomerController {
@Autowired
    private CustomerService customerService;
    @Autowired
    private  ResponseVo responseVo;
/*
* customer
* 封装所有页面信息
* */
@ApiOperation(value="注册新增用户",notes = "app端与pc端新用户注册")
    //@RequestMapping(value = "addCustomer",method = RequestMethod.POST)
   @PostMapping("addCustomer")
    public ResponseVo addCustomer(Customer customer)
    {

        try {
            responseVo=  customerService.addCustomer(customer);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }

      return   responseVo;
    }
   @ApiOperation(value = "app和pc用户登录",notes ="app和pc用户登录" )
   @PostMapping("login")
    public ResponseVo login(Customer customer)
   {
       try {
           responseVo=  customerService.login(customer);
       }
       catch (Exception e)
       {
           responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
           responseVo.setSuccess(false);
           responseVo.setMsg("系统异常");
           e.printStackTrace();
       }
       return   responseVo;
   }


    @ApiOperation(value = "pc用户退出",notes ="pc用户退出" )
    @GetMapping("loginOut")
    public ResponseVo loginOut(String userAccount)
    {
        try {
            responseVo=  customerService.loginOut(userAccount);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }
   //用户列表查询

    @ApiOperation(value = "用户列表查询",notes ="用户列表查询" )
    @PostMapping("listCustomers")
    public ResponseVo loginOut(Customer customer)
    {
        try {
            responseVo=  customerService.listCustomers(customer);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }

      //查询用户详情
    @ApiOperation(value = "查询用户详情",notes ="查询用户详情" )
    @GetMapping("findCustomerById")
    public ResponseVo queryUserDetails(Integer id)
    {
        try {
            responseVo=  customerService.queryUserDetails(id);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }

    @ApiOperation(value = "根据id修改信息",notes ="根据id修改信息" )
    @PostMapping("updateCustomerById")
    public ResponseVo updateCustomerById(Customer customer)
    {
        try {
            responseVo=  customerService.updateCustomerById(customer);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }



    @ApiOperation(value = "根据id删除用户信息",notes ="根据id删除用户信息" )
    @GetMapping("deleteCustomerById")
    public ResponseVo deleteCustomerById(Integer id)
    {
        try {
            responseVo=  customerService.deleteCustomerById(id);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }


    /*
    * String originPwd  原始密码
    * ,String newPwd：新密码
    * ,Integer userId,更改人的id
    * Integer userAccount 修改人的用户名
    *
    *
    * */

    @ApiOperation(value = "根据id更新用户密码",notes ="\"根据id更新用户密码" )
    @GetMapping("updatePwd")
    public ResponseVo updatePwd(String originPwd,String newPwd,Integer userId,String userAccount)
    {
        try {
            responseVo=  customerService.updatePwd(originPwd,newPwd,userId,userAccount);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }

    @ApiOperation(value = "根据id更新用户金额",notes ="根据id更新用户金额" )
    @GetMapping("updateScore")

    /*
    userId:修改的用户id
    ,String newScore:修改后的金额
    ,String userAccount:修改人
    * */
    public ResponseVo updatePwd(Integer userId,String newScore,String userAccount)
    {
        try {
            responseVo=  customerService.updateScore(newScore,userId,userAccount);
        }
        catch (Exception e)
        {
            responseVo.setCode(ErrorCode.SERVER_EXCEPTION_CODE);
            responseVo.setSuccess(false);
            responseVo.setMsg("系统异常");
            e.printStackTrace();
        }
        return   responseVo;
    }

}
