package com.neusoft.bookstore.customer.mapper;

import com.neusoft.bookstore.customer.model.Customer;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface CustomerMapper {


   
    int updateCustomerById(Customer customer) ;

    Customer findCustomerByPhoneAndAccount(Customer customer);

    int addCustomer(Customer customer);

    Customer selectLoginCustomer(Customer customer);

    List<Customer> listCustomers(Customer customer);

    Customer queryUserDetails(int id);

    List<Customer> findCustomerByPhoneAndAccountExOwn(Customer customer);

    int deleteCustomerById(Integer id);


    int updatePwdById(HashMap<Object, Object> hashMap);

    int updateScore(HashMap<Object, Object> map);
}
