package com.neusoft.bookstore.customer.model;

import com.neusoft.bookstore.util.BaseModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class Customer extends BaseModel {
    @ApiModelProperty("用户登录账号")
    private  String     userAccount;
    @ApiModelProperty("用户名称")
    private  String     userName;
    @ApiModelProperty("用户性别 0：女 1：男 2：未知")
    private  Integer    userSex;
    @ApiModelProperty("用户手机号")
    private  String     phone;
    @ApiModelProperty("用户邮箱")
    private  String     email;
    @ApiModelProperty("身份证")
    private  String     idCard;
    @ApiModelProperty("密码")
    private  String     password;
    @ApiModelProperty("积分")
    private  BigDecimal  score;
    @ApiModelProperty("登录源标记（0：表示app注册和登录1：表示从pc端登录注册）")
    private  Integer     isAdmin;
    @ApiModelProperty("从前台获取的积分金额")
    private  String frontScore;

}
